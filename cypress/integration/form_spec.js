describe('Tests for Form', function() {

  beforeEach(() => {
    cy.fixture('profile').then((profile) => {
      this.profile = profile;
    })
  });

  it('Tests content exists', function() {
    cy.visit('http://localhost:1234/form.html');

    cy.contains("I am form");
    cy.get('#firstName').should('not.have.value');
    cy.get('#email').should('not.have.value');
    cy.get('#result').should('be.empty');
  });

  it('Tests invalid inputs', () => {

    cy.get('#submitBtn').click();
    cy.get('#result').should('be.empty');
    cy.get('#firstName').should('have.class', 'error');
    cy.get('#email').should('have.class', 'error');

    cy.get('#firstName').type(this.profile.name);
    cy.get('#email').type('invalid email');
    cy.get('#submitBtn').click();
    cy.get('#result').should('be.empty');
    cy.get('#firstName').should('not.have.class', 'error');
    cy.get('#email').should('have.class', 'error');
  });

  it('Tests valid inputs', () => {
    cy.get('#firstName').type(this.profile.name);
    cy.get('#email').type(this.profile.email);
    cy.get('#submitBtn').click();
    cy.get('#result').contains('Success');
    cy.get('#firstName').should('not.have.class', 'error');
    cy.get('#email').should('not.have.class', 'error');
  });

});