#
HOW TO INSTALL

1. run `npm install`

#
HOW TO RUN

1. run `npm run start`
2. open URL `http://localhost:1234/`

#
HOW TO RUN TESTS
1. run `npm run test`
2. or `npm run test-open` to open Cypress



