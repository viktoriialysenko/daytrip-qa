describe('My First Test', function() {
  it('Tests for the text', function() {
    cy.visit('http://localhost:1234/');

    cy.contains("I am JavaScript!");
  });

  it('Tests for green button', () => {
    cy.visit('http://localhost:1234/');
    cy.get('.btnGroup').find('.myCustomButton').first().click();
    cy.on('window:alert', (str) => {
      expect(str).to.equal(`You clicked on myCustomButton with text Click me`)
    })
  });

  it('Tests for blue button', () => {
    cy.visit('http://localhost:1234/');
    cy.contains("You clicked on openModalBtn with text Open Modal").should('not.exist');
    cy.get('.btnGroup').find('#openModalBtn').as('blueBtn');
    cy.get('@blueBtn').click();
    cy.contains("You clicked on openModalBtn with text Open Modal").should('exist');
  });

  it('Tests for red button', () => {
    cy.visit('http://localhost:1234/');
    cy.contains('Click me and wait').click();
    cy.on('window:alert', (str) => {
      expect(str).to.equal(`Not correct string`)
    });
  });

});