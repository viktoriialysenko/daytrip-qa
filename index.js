document.getElementById("test").innerHTML = "Hello! I am JavaScript!";

document.querySelectorAll('.myCustomButton').forEach((element) => {
  element.onclick = (event) => {
    console.log('--------- step 1');
    console.log('I am "click on myCustomButton" event', event);

    alert('You clicked on myCustomButton with text ' + event.toElement.innerHTML);

    console.log('--------- step 2');
  };
});


document.getElementById("openModalBtn").onclick = (event) => {
  console.log('--------- step 1');
  console.log('I am "click on openModalBtn" event', event);

  document.getElementById('btnText').innerHTML = event.toElement.innerHTML;
  document.getElementById("myModal").style.display = "block";

  console.log('--------- step 2');
};


document.getElementById('superUniqueId').onclick = (event) => {
  console.log('--------- step 1');
  console.log('I am "click on superUniqueId" event', event);

  setTimeout(() => {
    console.log('--------- step 2');
    let btnText = event.toElement.innerHTML;
    document.getElementById('superUniqueId').innerHTML = "I was clicked";

    alert('You clicked on superUniqueId with text ' + btnText);
  }, 2000);

  console.log('--------- step 3');
};





(() => {
  // Get the modal
  const modal = document.getElementById("myModal");

// Get the <span> element that closes the modal
  const span = document.getElementsByClassName("close")[ 0 ];

// When the user clicks on <span> (x), close the modal
  span.onclick = function() {
    modal.style.display = "none";
  };

// When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
    if (event.target == modal) {
      modal.style.display = "none";
    }
  }
})();