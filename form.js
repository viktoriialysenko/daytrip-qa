
document.getElementById('submitBtn').onclick = () => {
  let firstName = document.getElementById('firstName').value;
  let email = document.getElementById('email').value;

  let _isFirstNameValid = isFirstNameValid(firstName);
  let _isEmailValid = isEmailValid(email);

  if (_isFirstNameValid) {
    document.getElementById('firstName').classList.remove("error");
  } else {
    document.getElementById('firstName').classList.add("error");
  }

  if (_isEmailValid) {
    document.getElementById('email').classList.remove("error");
  } else {
    document.getElementById('email').classList.add("error");
  }

  if (_isEmailValid && _isFirstNameValid) {
    document.getElementById('result').innerHTML = 'Success';
  }
};

function isFirstNameValid(firstName)
{
  return firstName && firstName.length > 0;
}

function isEmailValid(email)
{
  return email && email.length > 0 && /\S+@\S+\.\S+/.test(email);
}
